package exercise1;

public class Employee {
	public static void main(String[] args) {
		int salary=450;
	    int hours=8;
		Employee employee=new Employee();
		employee.getInfo(salary, hours);
	}
	
	public void getInfo(int sal,int hrs) {
		System.out.println("Employee salary is:"+sal);
		System.out.println("No of hours employee working per day:"+hrs);
		int finalSalary=sal+addSal(sal)+addWork(hrs);
		System.out.println("Final salary of the employee:"+finalSalary);
		
	}
	public static int addSal(int sal) {//if salary is less than $500 this method will return $10 else it will return $0
		int updatedSal=0;
		if(sal<500) {     
			updatedSal=10;
			System.out.println("Adding $10 to employee salary because Employee salary is less than $500.");
		}
		return updatedSal;
	}
	public static int addWork(int hrs) {//if number of hours is less than 6 per day then this method will return $5 else it will return $0
		int updatedSal=0;
		if(hrs>6) {
			updatedSal=5;
			System.out.println("Adding $5 to employee salary because Employeee worked more than 6 hours per day.");
		}
		return updatedSal;
	}

}
