package exercise1;

import java.util.Arrays;

public class ArrayAdd {

   public static void main(String[] args) {
   int a[]= {99,4,2,81,11,7,22};
   int target=9;
   boolean found=true;
   for(int i=0;i<a.length;i++)  
    {      
	   for(int j=i+1;j<a.length;j++)
	   {
		   int k=a[i]+a[j];
		   if(k==target)
		   {
			   System.out.println("Target found.");
			   int abc[]={i,j};
			   System.out.println(Arrays.toString(abc));
			   found=false;
		   break;
		   }  
	   }
    }
   if(found)
	   System.out.println("Target not found.");

}
}

/*
 * checking if number in one index plus number in any other index is equal to
 * the target number or not. If yes, then putting those two indices in an int
 * array and use Arrays.toString(which returns string representations of the
 * contents of the array) operation to print the output in the console. Using a
 * boolean variable to check if the target number is found or not.
 */