package exercise1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailLogin {
	public static void main(String args[]) {
	//setup path to the chrome driver
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Harsish\\eclipse-workspace\\Coding\\chromedriver\\chromedriver.exe");

	WebDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	WebDriverWait wait=new WebDriverWait(driver,20);
	driver.get(" https://www.gmail.com/");
	
	//Enter the UserName 
	WebElement username=driver.findElement(By.xpath("//input[@autocomplete=\"username\"]"));
	username.sendKeys("pls Enter your mail here");
	driver.findElement(By.xpath("//span[contains(text(),\"Next\")]")).click();
	
	//wait until next page is opened
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Forgot password?')]")));
	
	//Enter the password
	WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
	password.click();
	password.sendKeys("pls Enter your password here");
	driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
	driver.findElement(By.xpath("//span[contains(text(),\"Next\")]")).click();

	//wait until gmail is opened
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='gb_Ia gbii']")));
	
	//signout of gmail
	driver.findElement(By.xpath("//span[@class='gb_Ia gbii']")).click();
	driver.findElement(By.xpath("//a[contains(text(),'Sign out')]")).click();
	driver.close();	
	}

}
